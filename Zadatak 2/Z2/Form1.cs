﻿/* Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
 * svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
 * funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
 * dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z2
{
    public partial class Form1 : Form
    {
        List<String> listWords = new List<String>();
        List<Label> listLabels = new List<Label>();
        List<char> listSlova = new List<char>();
        string path = "C:\\words.txt";
        string mWord;
        int mTry = 5, mCounter = 0, tocno = 0;
       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listLabels.Add(label3);
            listLabels.Add(label4);
            listLabels.Add(label5);
            listLabels.Add(label6);
            listLabels.Add(label7);
            listLabels.Add(label8);
            listLabels.Add(label9);
            listLabels.Add(label10);
            listLabels.Add(label11);
            listLabels.Add(label12);
            listLabels.Add(label13);
            listLabels.Add(label14);
            listLabels.Add(label15);
            listLabels.Add(label16);
            listLabels.Add(label17);
            listLabels.Add(label18);

            gameInit();
        }

        private void provjera_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text))
            {
                if (mTry == 0 && !mWord.Contains(textBox1.Text))
                {
                    MessageBox.Show("Iskoristili ste sve pokušaje", "Game over!");
                    textBox1.Clear();
                    gameInit();
                }

                else if (mWord.Contains(textBox1.Text) && !listSlova.Contains(textBox1.Text[0]))
                {
                    listSlova.Add(textBox1.Text[0]);
                    listBox1.DataSource = null;
                    listBox1.DataSource = listSlova;
                    for (int i = 0; i < mWord.Length; i++)
                    {
                        if (mWord[i] == textBox1.Text[0])
                        {
                            listLabels[i].Text = textBox1.Text[0].ToString();
                            tocno++;
                        }
                    }

                    if (tocno == mWord.Length)
                    {
                        MessageBox.Show("Pogodili ste rijec", "Cestitamo!");
                        gameInit();
                    }
                }
                else if(!listSlova.Contains(textBox1.Text[0]))
                {
                    listSlova.Add(textBox1.Text[0]);
                    listBox1.DataSource = null;
                    listBox1.DataSource = listSlova;
                    mTry--;
                    label19.Text = "Preostali broj pokusaja: " + mTry.ToString();
                }
            }
            else
            {
                MessageBox.Show("Neispravan unos", "Greska!");
            }
            textBox1.Clear();
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void novaigra_Click(object sender, EventArgs e)
        {
            gameInit();
        }

        private void gameInit()
        {
            for(int i = 0; i < 14; i++)
            {
                listLabels[i].Text = "_";
                listLabels[i].Show();
            }

            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    listWords.Add(line);
                    mCounter++;
                }
            }

            Random rnd = new Random();
            int temp = rnd.Next(0, mCounter);
            mWord = listWords[temp];

            for(int i = 15; i >= mWord.Length; i--)
            {
                listLabels[i].Hide();
            }

            tocno = 0;
            mTry = 5;
            listSlova.Clear();
            label19.Text = "Preostali broj pokusaja: " + mTry.ToString();
            listBox1.DataSource = null;
            listBox1.DataSource = listSlova;
        }
    }
}
