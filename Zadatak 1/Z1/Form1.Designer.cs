﻿using System;

namespace Z1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cosine = new System.Windows.Forms.Button();
            this.tangens = new System.Windows.Forms.Button();
            this.cotangens = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.sine = new System.Windows.Forms.Button();
            this.korjen = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.divided = new System.Windows.Forms.Button();
            this.times = new System.Windows.Forms.Button();
            this.apsolute = new System.Windows.Forms.Button();
            this.power = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(30, 82);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(151, 28);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cosine
            // 
            this.cosine.Location = new System.Drawing.Point(265, 82);
            this.cosine.Margin = new System.Windows.Forms.Padding(4);
            this.cosine.Name = "cosine";
            this.cosine.Size = new System.Drawing.Size(45, 28);
            this.cosine.TabIndex = 8;
            this.cosine.Text = "cos";
            this.cosine.UseVisualStyleBackColor = true;
            this.cosine.Click += new System.EventHandler(this.cosine_Click);
            // 
            // tangens
            // 
            this.tangens.Location = new System.Drawing.Point(319, 82);
            this.tangens.Margin = new System.Windows.Forms.Padding(4);
            this.tangens.Name = "tangens";
            this.tangens.Size = new System.Drawing.Size(45, 28);
            this.tangens.TabIndex = 9;
            this.tangens.Text = "tan";
            this.tangens.UseVisualStyleBackColor = true;
            this.tangens.Click += new System.EventHandler(this.tangens_Click);
            // 
            // cotangens
            // 
            this.cotangens.Location = new System.Drawing.Point(372, 82);
            this.cotangens.Margin = new System.Windows.Forms.Padding(4);
            this.cotangens.Name = "cotangens";
            this.cotangens.Size = new System.Drawing.Size(45, 28);
            this.cotangens.TabIndex = 10;
            this.cotangens.Text = "ctg";
            this.cotangens.UseVisualStyleBackColor = true;
            this.cotangens.Click += new System.EventHandler(this.cotangens_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(319, 134);
            this.button10.Margin = new System.Windows.Forms.Padding(4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(99, 28);
            this.button10.TabIndex = 11;
            this.button10.Text = "=";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // sine
            // 
            this.sine.Location = new System.Drawing.Point(212, 82);
            this.sine.Margin = new System.Windows.Forms.Padding(4);
            this.sine.Name = "sine";
            this.sine.Size = new System.Drawing.Size(45, 28);
            this.sine.TabIndex = 12;
            this.sine.Text = "sin";
            this.sine.UseVisualStyleBackColor = true;
            this.sine.Click += new System.EventHandler(this.sine_Click);
            // 
            // korjen
            // 
            this.korjen.Location = new System.Drawing.Point(212, 134);
            this.korjen.Margin = new System.Windows.Forms.Padding(4);
            this.korjen.Name = "korjen";
            this.korjen.Size = new System.Drawing.Size(45, 28);
            this.korjen.TabIndex = 13;
            this.korjen.Text = "√";
            this.korjen.UseVisualStyleBackColor = true;
            this.korjen.Click += new System.EventHandler(this.korjen_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(212, 26);
            this.plus.Margin = new System.Windows.Forms.Padding(4);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(45, 28);
            this.plus.TabIndex = 14;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(265, 25);
            this.minus.Margin = new System.Windows.Forms.Padding(4);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(45, 28);
            this.minus.TabIndex = 15;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // divided
            // 
            this.divided.Location = new System.Drawing.Point(372, 25);
            this.divided.Margin = new System.Windows.Forms.Padding(4);
            this.divided.Name = "divided";
            this.divided.Size = new System.Drawing.Size(45, 28);
            this.divided.TabIndex = 16;
            this.divided.Text = "/";
            this.divided.UseVisualStyleBackColor = true;
            this.divided.Click += new System.EventHandler(this.divided_Click);
            // 
            // times
            // 
            this.times.Location = new System.Drawing.Point(319, 25);
            this.times.Margin = new System.Windows.Forms.Padding(4);
            this.times.Name = "times";
            this.times.Size = new System.Drawing.Size(45, 28);
            this.times.TabIndex = 17;
            this.times.Text = "*";
            this.times.UseVisualStyleBackColor = true;
            this.times.Click += new System.EventHandler(this.times_Click);
            // 
            // apsolute
            // 
            this.apsolute.Location = new System.Drawing.Point(265, 134);
            this.apsolute.Margin = new System.Windows.Forms.Padding(4);
            this.apsolute.Name = "apsolute";
            this.apsolute.Size = new System.Drawing.Size(45, 28);
            this.apsolute.TabIndex = 18;
            this.apsolute.Text = "aps";
            this.apsolute.UseVisualStyleBackColor = true;
            this.apsolute.Click += new System.EventHandler(this.apsolute_Click);
            // 
            // power
            // 
            this.power.Location = new System.Drawing.Point(106, 134);
            this.power.Name = "power";
            this.power.Size = new System.Drawing.Size(75, 28);
            this.power.TabIndex = 19;
            this.power.Text = "pow2";
            this.power.UseVisualStyleBackColor = true;
            this.power.Click += new System.EventHandler(this.power_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 233);
            this.Controls.Add(this.power);
            this.Controls.Add(this.apsolute);
            this.Controls.Add(this.times);
            this.Controls.Add(this.divided);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.korjen);
            this.Controls.Add(this.sine);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.cotangens);
            this.Controls.Add(this.tangens);
            this.Controls.Add(this.cosine);
            this.Controls.Add(this.textBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button cosine;
        private System.Windows.Forms.Button tangens;
        private System.Windows.Forms.Button cotangens;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button sine;
        private System.Windows.Forms.Button korjen;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button divided;
        private System.Windows.Forms.Button times;
        private System.Windows.Forms.Button apsolute;
        private System.Windows.Forms.Button power;
    }
}

