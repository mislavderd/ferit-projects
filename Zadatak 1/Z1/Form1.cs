﻿/* Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
 * znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
 * naprednih (sin, cos, log, sqrt...) operacija. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z1
{
    public partial class Form1 : Form
    {
        int mOption = -1;
        double mNum1, mNum2;

        public Form1()
        {
            InitializeComponent();
        }

        private void sine_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = Math.Sin(mNum1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void cosine_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = Math.Cos(mNum1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void tangens_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = Math.Tan(mNum1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void cotangens_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = (1 / Math.Tan(mNum1)).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void apsolute_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = Math.Abs(mNum1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Clear();
                mOption = 0;
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Clear();
                mOption = 1;
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void times_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Clear();
                mOption = 2;
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void divided_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Clear();
                mOption = 3;
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out mNum2))
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
                mOption = -1;
            }
            if (mOption == -1) textBox1.Clear();
            switch (mOption)
            {
                case 0:
                    textBox1.Text = (mNum1 + mNum2).ToString();
                    break;
                case 1:
                    textBox1.Text = (mNum1 - mNum2).ToString();
                    break;
                case 2:
                    textBox1.Text = (mNum1 * mNum2).ToString();
                    break;
                case 3:
                    textBox1.Text = (mNum1 / mNum2).ToString();
                    break;
            }
            mOption = -1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void power_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = Math.Pow(mNum1,2).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }

        private void korjen_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out mNum1))
            {
                textBox1.Text = Math.Sqrt(mNum1).ToString();
            }
            else
            {
                MessageBox.Show("Pogrešan unos broja", "Pogreška!");
                textBox1.Clear();
            }
        }
    }
}
