﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ticTacToe
{
    public partial class Form1 : Form
    {
        bool turn = true;
        int turn_count = 0;
        int x_count = 0;
        int o_count = 0;
        int draw_count = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button B = (Button)sender;
            if (turn)
                B.Text = "X";
            else
                B.Text = "O";
            turn = !turn;
            B.Enabled = false;
            turn_count++;

            checkWinner();
        }

        private void checkWinner()
        {
            bool winner = false;
            
            //HORIZONTAL START
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text)&&(!A1.Enabled))
            {
                winner = true;
            }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
            {
                winner = true;
            }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
            {
                winner = true;
            }
            //HORIZONTAL END

            //DIAGONAL START
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
            {
                winner = true;
            }
            else if ((C1.Text == B2.Text) && (B2.Text == A3.Text) && (!C1.Enabled))
            {
                winner = true;
            }
            //DIAGONAL END

            //VERTICAL START
            else if ((C1.Text == B1.Text) && (B1.Text == A1.Text) && (!C1.Enabled))
            {
                winner = true;
            }
            else if ((C2.Text == B2.Text) && (B2.Text == A2.Text) && (!C2.Enabled))
            {
                winner = true;
            }
            else if ((C3.Text == B3.Text) && (B3.Text == A3.Text) && (!C3.Enabled))
            {
                winner = true;
            }
            //VERTICAL END

            if (winner)
            {
                disableButtons();
                String champion = "";
                if (turn)
                {
                    champion = "O";
                    o_count++;
                    labelY.Text = o_count.ToString();
                }
                else
                {
                    champion = "X";
                    x_count++;
                    labelX.Text = x_count.ToString();
                }
                MessageBox.Show(champion + " IS A WINNER!", "HOORAY");
            }
            else
            {
                if (turn_count == 9)
                {
                    MessageBox.Show("Draw.", "Result");
                    draw_count++;
                    labelDRAW.Text = draw_count.ToString();
                }
            }

        }
        private void disableButtons()
        {
            foreach (Control c in Controls)
            {
                if (c.GetType() == typeof(Button))
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                    button2.Enabled = true;
                    button1.Enabled = true;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;
            foreach (Control c in Controls)
            {
                if (c.GetType() == typeof(Button))
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                    button2.Text = "New game";
                    button1.Text = "EXIT";
                }
            }

        }

        private void butt_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (b.Enabled)
            {

                if (turn)
                    b.Text = "X";
                else
                    b.Text = "O";
            }
        }

        private void butt_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
